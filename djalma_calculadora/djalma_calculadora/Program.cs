﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace djalma_calculadora
{
    internal class Program
    {
        static void Main(string[] args)
        {

            // Criar uma calculadora simples.

            int operacao = 0;
            string num1, num2;
            double num1real = 0;
            double num2real = 0;
            double resultado = 0;
            bool isvalid1 = false;
            bool isvalid2 = false;

            Console.WriteLine("Qual será a operação matemática a ser realizada?");
            Console.WriteLine("1 = adição");
            Console.WriteLine("2 = subtração");
            Console.WriteLine("3 = multiplicação");
            Console.WriteLine("4 = divisão");
            operacao = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Insira o primeiro número.");
            num1 = Console.ReadLine();
            isvalid1 = Double.TryParse(num1, out num1real);

            while (!isvalid1)
            {
                Console.WriteLine("Insira um número VÁLIDO.");
                num1 = Console.ReadLine();
                isvalid1 = Double.TryParse(num1, out num1real);
            }

            Console.WriteLine("Insira o segundo número.");
            num2 = Console.ReadLine();
            isvalid2 = Double.TryParse(num2, out num2real);

            while (!isvalid2)
            {
                Console.WriteLine("Insira um número VÁLIDO.");
                num2 = Console.ReadLine();
                isvalid2 = Double.TryParse(num2, out num2real);
            }


            switch (operacao)
            {
                case 1:

                    resultado = num1real + num2real;
                    break;

                case 2:

                    resultado = num1real - num2real;
                    break;

                case 3:
                    resultado = num1real * num2real;
                    break;

                case 4:
                    resultado = num1real / num2real;
                    break;

                default:
                    Console.WriteLine("Tente novamente, preste atenção nas instruções.");
                    break;

            }

            Console.WriteLine("RESULTADO: \n" + resultado.ToString("0.000"));
            Console.WriteLine("\nPressione qualquer tecla para finalizar.");
            Console.ReadKey();

        }
    }
}
