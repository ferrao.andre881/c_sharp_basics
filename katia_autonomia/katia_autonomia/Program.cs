﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace katia_autonomia
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //Receber tipo de combustível, distância percorrida. calcular consumo de combustível.
            //Autonomia gasolina: 10km/l  e Etanol: 8.5km/l

            string combustivelInput, distInput;
            double dist = 0;
            double combustivel;
            double autonomia = 0;
            double consumo = 0;
            bool isvalid6 = false;
            bool isvalid7 = false;

            Console.WriteLine("Qual é o tipo de combustível?");
            Console.WriteLine("1 - GASOLINA");
            Console.WriteLine("2 - ETANOL");
            combustivelInput = Console.ReadLine();
            isvalid6 = Double.TryParse(combustivelInput, out combustivel);

            while (combustivel > 2 || combustivel < 1)
            {
                Console.WriteLine("\nERRO! INSIRA UM VALOR VÁLIDO!");
                Console.WriteLine("Qual é o tipo de combustível?");
                Console.WriteLine("1 - GASOLINA");
                Console.WriteLine("2 - ETANOL");
                combustivelInput = Console.ReadLine();
                isvalid6 = Double.TryParse(combustivelInput, out combustivel);
            }

            while (!isvalid6)
            {
                Console.WriteLine("\nERRO! INSIRA UM VALOR VÁLIDO!");
                Console.WriteLine("Qual é o tipo de combustível?");
                Console.WriteLine("1 - GASOLINA");
                Console.WriteLine("2 - ETANOL");
                combustivelInput = Console.ReadLine();
                isvalid6 = Double.TryParse(combustivelInput, out combustivel);
            }

            Console.WriteLine("\nInsira a distância percorrida em QUILOMETROS!");
            distInput = Console.ReadLine();
            isvalid7 = Double.TryParse(distInput, out dist);

            while (!isvalid7)
            {
                Console.WriteLine("\nInsira uma distância percorrida VÁLIDA em QUILOMETROS!");
                distInput = Console.ReadLine();
                isvalid7 = Double.TryParse(distInput, out dist);
            }

            switch (combustivel)
            {
                case 1:
                    autonomia = 10;
                    consumo = dist / autonomia;
                    Console.WriteLine("\nO consumo de gasolina para percorrer " + dist.ToString("0.00") + "km é de " + consumo.ToString("0.00") + " litros.");
                    break;

                case 2:
                    autonomia = 8.5;
                    consumo = dist / autonomia;
                    Console.WriteLine("\nO consumo de etanol para percorrer " + dist.ToString("0.00") + "km é de " + consumo.ToString("0.00") + " litros.");
                    break;

                default:
                    Console.WriteLine("\nAlgo deu errado, tente novamente.");
                    break;
            }

            Console.WriteLine("\nPressione qualquer tecla para encerrar.");
            Console.ReadKey();
        }
    }
}
