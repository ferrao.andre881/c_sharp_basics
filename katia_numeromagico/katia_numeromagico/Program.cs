﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace katia_numeromagico
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //Fazer um programa que recebe um numero de quatro digitos e mostre se o número é magico. Separa-se os dois
            //primeiros digitos dos dois ultimos, soma-se e o resutlado eleve a 2. Exemplo: 3025.
            // Separe-se 30 e 25, depois 30 + 25 e por fim, 55*55 = 3025.

            string numInput;
            bool isvalid = false;
            double numValid = 0;
            int numero, metadeIni, metadeFim, somaNum;

            Console.WriteLine("Insira um numero com quatro digitos.\n");
            numInput = Console.ReadLine();
            isvalid = Double.TryParse(numInput, out numValid);

            while (numInput.Length !=4 || !isvalid)
            {

                Console.WriteLine("\nInsira um numero VÁLIDO com quatro digitos.");
                numInput = Console.ReadLine();
                isvalid = Double.TryParse(numInput, out numValid);

            }

            numero = Convert.ToInt32(numInput);
            metadeIni = numero / 100;
            metadeFim = numero % 100;
            somaNum = metadeIni + metadeFim;

            if (numero == (somaNum * somaNum))
            { 

                Console.WriteLine("\nVamos lá, você escolheu o numero " + numero + ". Ao separarmos ele em dois números,");
                Console.WriteLine("teremos " + metadeIni + " e " + metadeFim + ". Ao somar estes números, teremos " + somaNum+".");
                Console.WriteLine("E se o elevarmos ao quadrado, teremos " + (somaNum * somaNum) + ". Portanto, este numero é magico.");

            }

            else
            {

                Console.WriteLine("\nVamos lá, você escolheu o numero " + numero + ". Ao separarmos ele em dois números,");
                Console.WriteLine("teremos " + metadeIni + " e " + metadeFim + ". Ao somar estes números, teremos " + somaNum + ".");
                Console.WriteLine("E se o elevarmos ao quadrado, teremos " + (somaNum * somaNum) + ". Portanto, este numero NÃO é magico.");

            }

            Console.WriteLine("\nPressione qualquer tecla para encerrar.");
            Console.ReadKey();

        }
    }
}
