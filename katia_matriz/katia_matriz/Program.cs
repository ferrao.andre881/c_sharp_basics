﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace katia_matriz
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //Fazer um programa que armazena peso de trinta bois em uma matriz 5x6, após armazenar, mostrar o peso e a
            //posição do boi mais pesado, do boi mais leve e também a média de peso de todos os bois.

            int linha = 5;
            int coluna = 6;
            double[,] matriz = new double[linha, coluna];
            double maisPesado = 0, maisLeve = 100000000000000000, mediaPeso;
            double somaPeso = 0;


            Console.WriteLine("Insira o peso de cada boi. Aperte a tecla ENTER após inserir o peso de CADA boi.");

            for (int l = 0; l < linha; l++)
            {

                for (int c = 0; c < coluna; c++)
                {
                    matriz[l, c] = Double.Parse(Console.ReadLine());

                    somaPeso += matriz[l, c];

                    if (matriz[l, c] > maisPesado)
                    {
                        maisPesado = matriz[l, c];
                    }

                    if (matriz[l, c] < maisLeve)
                    {
                        maisLeve = matriz[l, c];
                    }

                }

            }

            for (int l = 0; l < linha; l++)
            {

                Console.WriteLine("");

                for (int c = 0; c < coluna; c++)
                {
                    Console.Write(" | " + matriz[l, c]);

                }

            }

            mediaPeso = somaPeso / 30;

            Console.WriteLine("\n*************************************");
            Console.WriteLine($" Boi mais pesado: {maisPesado.ToString("0.00")}Kg.");
            Console.WriteLine($" Bois mais leve: {maisLeve.ToString("0.00")}Kg.");
            Console.WriteLine($" MEDIA: {mediaPeso.ToString("0.00")}Kg.\n*************************************");
            Console.WriteLine("\n Pressione qualquer tecla para encerrar.");
            Console.ReadKey();

        }
    }
}
