﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace katia_crescente
{
    internal class Program
    {
        static void Main(string[] args)
        {

            string a, b, c;
            double aa = 0;
            double bb = 0;
            double cc = 0;
            bool isvalid3 = false;
            bool isvalid4 = false;
            bool isvalid5 = false;

            Console.WriteLine("Insira um numero qualquer.");
            a = Console.ReadLine();
            isvalid3 = Double.TryParse(a, out aa);

            while (!isvalid3)
            {
                Console.WriteLine("\nInsira um numero VÁLIDO qualquer.");
                a = Console.ReadLine();
                isvalid3 = Double.TryParse(a, out aa);
            }

            Console.WriteLine("\nInsira outro numero qualquer.");
            b = Console.ReadLine();
            isvalid4 = Double.TryParse(b, out bb);

            while (!isvalid4)
            {
                Console.WriteLine("\nInsira outro numero VÁLIDO qualquer.");
                b = Console.ReadLine();
                isvalid4 = Double.TryParse(b, out bb);
            }

            Console.WriteLine("\nInsira um ultimo numero qualquer.");
            c = Console.ReadLine();
            isvalid5 = Double.TryParse(c, out cc);

            while (!isvalid5)
            {
                Console.WriteLine("\nInsira um ultimo numero VÁLIDO qualquer.");
                c = Console.ReadLine();
                isvalid5 = Double.TryParse(c, out cc);

            }

            if (aa < bb && bb < cc)
            {
                Console.WriteLine("\na ordem crescente dos números é de " + aa + " " + bb + " " + cc + ".");
            }
            else if (aa < cc && cc < bb)
            {
                Console.WriteLine("\na ordem crescente dos números é de " + aa + " " + cc + " " + bb + ".");
            }
            else if (bb < aa && aa < cc)
            {
                Console.WriteLine("\na ordem crescente dos números é de " + bb + " " + aa + " " + cc + ".");
            }
            else if (bb < cc && cc < aa)
            {
                Console.WriteLine("\na ordem crescente dos números é de " + bb + " " + cc + " " + aa + ".");
            }
            else if (cc < aa && aa < bb)
            {
                Console.WriteLine("\na ordem crescente dos números é de " + cc + " " + aa + " " + bb + ".");
            }
            else if (cc < bb && bb < aa)
            {
                Console.WriteLine("\na ordem crescente dos números é de " + cc + " " + bb + " " + aa + ".");
            }

            Console.WriteLine("\nPressione qualquer tecla para encerrar.");
            Console.ReadKey();
        }
    }
}
