﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dia_semana
{
    internal class Program
    {
        static void Main(string[] args)
        {

            int diaInput;

            Console.WriteLine("Insira um número de 1 a 7.");
            diaInput = int.Parse(Console.ReadLine());

            switch (diaInput)
            {
                case 1:
                    Console.WriteLine("Domingo");
                    break;

                case 2:
                    Console.WriteLine("Segunda Feira");
                    break;

                case 3:
                    Console.WriteLine("Terça Feira");
                    break;

                case 4:
                    Console.WriteLine("Quarta Feira");
                    break;

                case 5:
                    Console.WriteLine("Quinta Feira");
                    break;

                case 6:
                    Console.WriteLine("Sexta Feira");
                    break;

                case 7:
                    Console.WriteLine("Sábado");
                    break;

                default:
                    Console.WriteLine("Você falhou ao ler as instruções, tente novamente.");
                    break;

            }

            Console.WriteLine("Pressione qualquer tecla para encerrar.");
            Console.ReadKey();

        }
    }
}
