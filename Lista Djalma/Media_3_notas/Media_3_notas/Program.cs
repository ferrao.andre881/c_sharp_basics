﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Media_3_notas
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double nota1Input = 0, nota2Input = 0, nota3Input = 0;
            double nota1, nota2, nota3;
            double media;

            Console.WriteLine("Insira a primeira nota. (USE APENAS NÚMEROS)");
            nota1Input = Double.Parse(Console.ReadLine());
            nota1 = nota1Input * 3;

            Console.WriteLine("Insira a segunda nota. (USE APENAS NÚMEROS)");
            nota2Input = Double.Parse(Console.ReadLine());
            nota2 = nota2Input * 3;

            Console.WriteLine("Insira a terceira nota. (USE APENAS NÚMEROS)");
            nota3Input = Double.Parse(Console.ReadLine());
            nota3 = nota3Input * 4;

            media = (nota1 + nota2 + nota3) / 10;

            Console.WriteLine($"Sua média foi: {media}.");
            Console.WriteLine("Pressione qualquer tecla para encerrar.");
            Console.ReadKey();

        }
    }
}
