﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Idade
{
    internal class Program
    {
        static void Main(string[] args)
        {

            int idade;

            Console.WriteLine("Insira sua idade. (USE APENAS NÚMEROS)");
            idade = int.Parse(Console.ReadLine());

            if (idade < 18)
            {
                Console.WriteLine("Você é menor de idade.");
            }

            else if (idade >= 18 && idade < 65)
            {
                Console.WriteLine("Você é um adulto.");
            }

            else
                Console.WriteLine("Você é idoso.");

            Console.WriteLine("Pressione qualquer tecla para encerrar.");
            Console.ReadKey();

        }
    }
}
