﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace salvar_dados_txt
{
    internal class Program
    {
        static void Main(string[] args)
        {
           
            string nome, idade, texto;
            var arquivo = "C:\\Ex Djalma\\dados.txt";

            Console.WriteLine("Insira seu nome");
            nome = Console.ReadLine();

            Console.Clear();

            Console.WriteLine("Insira sua idade.");
            idade = Console.ReadLine();

            Console.Clear();

            texto = "Nome:" + nome + ", idade: " + idade;

            File.WriteAllText(arquivo, texto);

            Console.WriteLine($"{nome}, tem {idade} anos de idade.\nPressione qualquer tecla para encerrar.");
            Console.ReadKey();
        }
    }
}
