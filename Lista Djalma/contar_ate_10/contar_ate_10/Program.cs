﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace contar_ate_10
{
    internal class Program
    {
        static void Main(string[] args)
        {

            int i = 1;

            do
            {
                Console.WriteLine($"{i}");
                i++;
            } while (i <= 10);

            Console.WriteLine("Pressione qualquer tecla para encerrar.");
            Console.ReadKey();

        }
    }
}
