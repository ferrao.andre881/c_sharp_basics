﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matriz
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[,] numeros = new int[3, 3];


            for (int i = 0; i < numeros.GetLength(0); i++)
            {
                for (int j = 0; j < numeros.GetLength(1); j++)
                {
                    Console.WriteLine("Digite um numero qualquer.");
                    numeros[i, j] = int.Parse(Console.ReadLine());
                }
            }

            Console.WriteLine("Aqui está sua matriz numérica:\n");

            for (int i = 0; i < numeros.GetLength(0); i++)
            {
                Console.WriteLine($"{numeros[i, 0]} | {numeros[i, 1]} | {numeros[i, 2]}");
            }

            Console.WriteLine("\nPressione qualquer tecla para continuar.");
            Console.ReadKey();
        }
    }
}
