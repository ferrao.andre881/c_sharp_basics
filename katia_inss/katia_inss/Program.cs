﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace katia_inss
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //Pegar o cargo e o salário. Salário até 3000 aliquota INSS = 10%. Acima disso, aliquota = 14%

            double salario;
            string cargo, salarioInput;
            bool isValid2 = false;
            salario = 0;

            Console.WriteLine("Insira o seu cargo.");
            cargo = Console.ReadLine();

            Console.WriteLine("Insira o seu salário.");
            salarioInput = Console.ReadLine();
            isValid2 = Double.TryParse(salarioInput, out salario);

            while (!isValid2)
            {
                Console.WriteLine("Insira um numero VÁLIDO para seu salário.");
                salarioInput = Console.ReadLine();
                isValid2 = Double.TryParse(salarioInput, out salario);
            }

            if (salario <= 3000)
            {
                double aliquota = 0.9;
                Console.WriteLine("Com o desconto do INSS, você receberá, líquido, o valor de: R$" + (salario * aliquota).ToString("0.00") + ".");

            }

            else
            {
                double aliquota = 0.86;
                Console.WriteLine("Com o desconto do INSS, você receberá, líquido, o valor de: R$" + (salario * aliquota).ToString("0.00") + ".");

            }

            Console.WriteLine("\nPressione qualquer tecla para encerrar");
            Console.ReadKey();
        }
    }
}
