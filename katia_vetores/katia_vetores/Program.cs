﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace katia_vetores
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Basico sobre Array

            //int n1, n2, n3, n4, n5;
            //int[] n = new int[5];
            //int[] num = new int[3] { 55, 77, 99 };
            //int[] num = { 55, 77, 99, 66, 88 };



            //n[0] = 11;
            //n[1] = 22;
            //n[2] = 33;
            //n[3] = 44;
            //n[4] = 55;

            //Array.ForEach(num, Console.WriteLine);
            //Console.WriteLine("[{0}]", string.Join(", ",num));
            //Console.ReadKey();

            #endregion

            #region Ex Katia

            int[] arr1 = new int[5];
            int[] arr2 = new int[5];
            int[] arr3 = new int[10];
            int j = 0;

            Console.WriteLine("\nArray 1: Digite cinco numeros. Aperte a tecla ENTER após cada um deles.\n");

            for (int p = 0; p < 5; p++) 
            {
                arr1[p] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("\nArray 2: Digite novamente cinco numeros. Aperte a tecla ENTER após cada um deles.\n");

            for (int q = 0; q < 5; q++)
            {
                arr2[q] = Convert.ToInt32(Console.ReadLine());
            }

            //int tamanho = arr1.Length + arr2.Length;

            for (int i = 0; i < 5; i++)
            {

                arr3[j++] = arr1[i];
                arr3[j++] = arr2[i];
            }

            Console.WriteLine(" ");
            Array.ForEach(arr3, Console.WriteLine);

            Console.WriteLine("\nPressione qualquer tecla para encerrar.");
            Console.ReadKey();




            #endregion
        }
    }
}
