﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace katia_fibonacci
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //Sequência Fibonacci onde o próximo número é a soma dos dois números anteriores.


            Console.WriteLine("Insira um número maior que três.");
            int inputUser = int.Parse(Console.ReadLine());

            if (inputUser < 3)
            {
                Console.WriteLine("Você deve informar um número maior que três!");
                return;
            }

            else
            {
                int[] fibonacci = new int[inputUser];
                fibonacci[0] = 1;
                fibonacci[1] = 1;

                for (int i = 2; i < inputUser; i++)
                {
                    fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
                }

                Console.WriteLine($"A sequência Fibonacci ficou:");

                for (int i = 0; i < inputUser; i++)
                {
                    Console.Write($"{fibonacci[i]}, ");
                }

                Console.WriteLine("\nPressione qualquer tecla para finalizar.");
                Console.ReadKey();
            }
        }
    }
}
