﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace katia_idadeEGenero
{
    internal class Program
    {
        static void Main(string[] args)
        {

            // Faça um programa que armazena dados de 100 pessoas. Como entrada o algoritmo
            // receberá a idade em um vetor e sexo em um outro vetor. Após armazenar os dados,
            // mostre o percentual de mulheres entre 25 e 50 anos, o percentual de homens entre 50 e
            // 75 anos e a média de idades de mulheres e homens.
            // Mulher = 1; Homem = 0;

            int[,] arr1 = new int[1000, 2];

            Random idade = new Random();
            Random gen = new Random();
            int contM = 0, idadeM = 0, contH = 0, idadeH = 0, contIdadeM = 0, contIdadeH = 0;

            for (int i = 0; i < arr1.GetLength(0); i++)
            {
                arr1[i, 0] = idade.Next(23, 76);
                arr1[i, 1] = gen.Next(0, 2);

                int idadeMatriz = arr1[i,0];
                bool mulher = arr1[i, 1] == 1;

                if (mulher)
                {
                    contM++;
                    idadeM = idadeM + idadeMatriz;

                    if (idadeMatriz >= 25 && idadeMatriz <= 50)
                    {
                        contIdadeM = contIdadeM + 1;
                    }
                }

                if (!mulher)
                {
                    contH++;
                    idadeH = idadeH + idadeMatriz;

                    if (idadeMatriz >= 50 && idadeMatriz <= 75)
                    {
                        contIdadeH = contIdadeH + 1;
                        
                    }
                }                                             
            }


            for (int i = 0; i < arr1.GetLength(0); i++)
            {
                Console.WriteLine($"{arr1[i, 0]} | {arr1[i, 1]}");
            }


            double percentH = Convert.ToDouble(contIdadeH) / Convert.ToDouble(contH) * 100;
            double percentM = Convert.ToDouble(contIdadeM) / Convert.ToDouble(contM) * 100;

            double mediaIdH = idadeH / contH;
            double mediaIdM = idadeM / contM;

            Console.WriteLine("contIdadeM: " + contIdadeM);
            Console.WriteLine("contIdadeH: " + contIdadeH);
            Console.WriteLine($"\nMulheres: {contM}, Homens: {contH}");
            Console.WriteLine($"Média de idade das mulheres: {mediaIdM.ToString("0.00")}");
            Console.WriteLine($"Média de idade dos homens: {mediaIdH.ToString("0.00")}");
            Console.WriteLine($"Percentual homens entre 50 e 75 anos: {percentH.ToString("0.00")}%.");
            Console.WriteLine($"Percentual mulheres entre 25 e 50 anos: {percentM.ToString("0.00")}%.");

            Console.ReadKey();
        }
    }
}
