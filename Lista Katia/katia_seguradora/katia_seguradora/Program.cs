﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace katia_seguradora
{
    internal class Program
    {
        static void Main(string[] args)
        {

            // Uma Seguradora está concedendo descontos dependendo da idade e sexo de seus
            //assegurados.Escreva um programa que leia o nome, a idade e o sexo do assegurado e
            //mostre o desconto concedido em porcentagem segundo a tabela:
            // Mulheres: 18 a 25 anos = 4%, 26 a 55 anos = 7%, acima 55 anos = 10%
            // Homens: 18 a 25 anos = 3%, 26 a 55 anos = 6%, acima de 55 anos = 9%

            string nome, genero;
            int idade;
            bool mulher = false;
            Console.WriteLine("Qual é o seu nome?");
            nome = Console.ReadLine();

            Console.Clear();

            Console.WriteLine("Informe sua idade:\n");
            idade = int.Parse(Console.ReadLine());

            Console.Clear();

            Console.WriteLine("Informe o genero ao qual você se identifica:");
            Console.WriteLine("M para masculino");
            Console.WriteLine("F para feminino");
            genero = Console.ReadLine();
            genero = genero.ToUpper();

            mulher = genero == "F";
            
            if (mulher && idade > 55)
            {
                Console.WriteLine($"Olá {nome}, de acordo com seus dados, aos {idade} anos, seu desconto é de 10%. Aproveite!");
            }

            else if (mulher && idade >= 26 && idade <= 55)
            {
                Console.WriteLine($"Olá {nome}, de acordo com seus dados, aos {idade} anos,  seu desconto é de 7%. Aproveite!");
            }

            else if (mulher && idade >= 18 && idade <= 25)
            {
                Console.WriteLine($"Olá {nome}, de acordo com seus dados, aos {idade} anos,  seu desconto é de 4%. Aproveite!");
            }

            else if (!mulher && idade > 55)
            {
                Console.WriteLine($"Olá {nome}, de acordo com seus dados, aos {idade} anos,  seu desconto é de 9%. Aproveite!");
            }

            else if (!mulher && idade >= 26 && idade <= 55)
            {
                Console.WriteLine($"Olá {nome}, de acordo com seus dados, aos {idade} anos,  seu desconto é de 6%. Aproveite!");
            }

            else if (!mulher && idade >= 18 && idade <= 25)
            {
                Console.WriteLine($"Olá {nome}, de acordo com seus dados, aos {idade} anos,  seu desconto é de 3%. Aproveite!");
            }

            Console.WriteLine("\nPressione qualquer tecla para encerrar.");
            Console.ReadKey();

        }
    }
}
