﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace katia_armazem
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //Faça um programa que receba o estoque atual de três produtos, armazenados em quatro
            //armazéns e coloque esses dados em uma matriz 4 x 3. Em seguida, o programa deverá
            //calcular e mostrar:
            //1- a quantidade de itens armazenados em cada armazém;
            //2- qual armazém possui maior estoque do produto 2;
            //3- qual armazém possui menor estoque do produto 1.


            int[,] matrix1 = new int[4, 3];
            int arm = matrix1.GetLength(0);
            int prod = matrix1.GetLength(1);
            int arm1, arm2, arm3, arm4;
            Random quantia = new Random();

            for (int i = 0; i < arm; i++)
            {
                for (int j = 0; j < prod; j++)
                {
                    matrix1[i, j] = quantia.Next(50, 300);
                }
            }

            arm1 = matrix1[0, 0] + matrix1[0, 1] + matrix1[0, 2];
            arm2 = matrix1[1, 0] + matrix1[1, 1] + matrix1[1, 2];
            arm3 = matrix1[2, 0] + matrix1[2, 1] + matrix1[2, 2];
            arm4 = matrix1[3, 0] + matrix1[3, 1] + matrix1[3, 2];

            for (int i = 0; i < arm; ++i)
            {
                Console.WriteLine($"Armazém {i + 1}: {matrix1[i, 0]} | {matrix1[i, 1]} | {matrix1[i, 2]}");
            }

            //--------Total de Itens-----------//

            Console.WriteLine($"\nO total de itens em cada armazém é de:");
            Console.WriteLine($"Armazém 1: {arm1}, Armazém 2: {arm2}, Armazém 3: {arm3} e Armazém 4: {arm4}");


            //-----------Produto 1-------------//

            if (matrix1[0, 0] > matrix1[1, 0] && matrix1[0, 0] > matrix1[2, 0] && matrix1[0, 0] > matrix1[3, 0])
            {
                Console.WriteLine($"\nO armazém 1 é o que possui mais produtos do tipo 1. Com {matrix1[0, 0]} unidades.");
            }

            else if (matrix1[1, 0] > matrix1[0, 0] && matrix1[1, 0] > matrix1[2, 0] && matrix1[1, 0] > matrix1[3, 0])
            {
                Console.WriteLine($"\nO armazém 2 é o que possui mais produtos do tipo 1. Com {matrix1[1, 0]} unidades.");
            }

            else if (matrix1[2, 0] > matrix1[1, 0] && matrix1[2, 0] > matrix1[0, 0] && matrix1[2, 0] > matrix1[3, 0])
            {
                Console.WriteLine($"\nO armazém 3 é o que possui mais produtos do tipo 1. Com {matrix1[2, 0]} unidades.");
            }

            else
            {
                Console.WriteLine($"\nO armazém 4 é o que possui mais produtos do tipo 1. Com {matrix1[3, 0]} unidades.");
            }

            //-----------Produto 2---------------//

            if (matrix1[0, 1] > matrix1[1, 1] && matrix1[0, 1] > matrix1[2, 1] && matrix1[0, 1] > matrix1[3, 1])
            {
                Console.WriteLine($"\nO armazém 1 é o que possui mais produtos do tipo 2. Com {matrix1[0, 1]} unidades.");
            }

            else if (matrix1[1, 1] > matrix1[0, 1] && matrix1[1, 1] > matrix1[2, 1] && matrix1[1, 1] > matrix1[3, 1])
            {
                Console.WriteLine($"\nO armazém 2 é o que possui mais produtos do tipo 2. Com {matrix1[1, 1]} unidades.");
            }

            else if (matrix1[2, 1] > matrix1[0, 1] && matrix1[2, 1] > matrix1[1, 1] && matrix1[2, 1] > matrix1[3, 1])
            {
                Console.WriteLine($"\nO armazém 3 é o que possui mais produtos do tipo 2. Com {matrix1[2, 1]} unidades.");
            }

            else
            {
                Console.WriteLine($"\nO armazém 4 é o que possui mais produtos do tipo 2. Com {matrix1[3, 1]} unidades.");
            }


            Console.ReadKey();

        }
    }
}
