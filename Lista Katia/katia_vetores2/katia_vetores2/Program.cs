﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace katia_vetores2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Existe um marciano escondido em uma árvore numa floresta composta de 100 arvores que ficam lado a lado.
            // Um caçador tem 5 tiros para tentar acertar a árvore onde o marciano está. O marciano por sua vez, dará
            // dicas de sua localização. Caso as balas acabem, o Marciano atacará o caçador.

            Random marciano = new Random();
            int[] floresta = new int[100] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100 };
            int posicao = marciano.Next(1, 100);
            int palpite;
            int tentativa = 0;
            int dificuldade = 0;


            Console.WriteLine("*******************************************************");
            Console.WriteLine("Existe um marciano escondido em uma destas 100 árvores.");
            Console.WriteLine("****************ONDE SERÁ QUE ELE ESTÁ?****************");
            Console.WriteLine("*******************************************************");


            Console.WriteLine("Selecione o nível de dificuldade:");
            Console.WriteLine("1- Fácil");
            Console.WriteLine("2- Intermediário");
            Console.WriteLine("3- Difícil");
            Console.Write("Sua escolha:");

            dificuldade = int.Parse(Console.ReadLine());

            if (dificuldade == 1)
            {
                tentativa = 15;
                Console.WriteLine("Você escolheu a dificuldade fácil!\n");
            }

            else if (dificuldade == 2)
            {
                tentativa = 10;
                Console.WriteLine("Você escolheu a dificuldade intermediária!\n");
            }

            else if (dificuldade == 3)
            {
                tentativa = 5;
                Console.WriteLine("Você escolheu a dificuldade difícil!\n");
            }

            Console.WriteLine("*************************************");


            for (int i = 0; i < tentativa; i++)
            {

                Console.WriteLine($"\nMunição restante:{tentativa - i}.");
                Console.Write("Qual é o seu palpite?\nPalpite: ");
                palpite = int.Parse(Console.ReadLine());


                if (floresta[palpite] < posicao)
                {
                    Console.WriteLine("Você errou, estou mais para a direita!");
                }

                else if (floresta[palpite] > posicao)
                {
                    Console.WriteLine("Você errou, estou mais para a esquerda!");
                }

                else if (floresta[palpite] == posicao)
                {
                    Console.WriteLine("AI! QUE HORROR! VOCÊ ACERTOU O MARCIANO EM CHEIO!\nAgora as chances da humanidade evoluir como espécie se foram, \nestamos fadados a extinção e a culpa do sangue alienígena nas mãos é toda sua!");
                    Console.WriteLine("(Você venceu, mas a que custo?)");
                    Console.WriteLine("Pressione algo para encerrar.");
                    Console.ReadKey();
                    return;
                }

            }

            Console.WriteLine("SUA MUNIÇÃO ACABOU E AGORA EU TE PEGO E TE JOOJ JOOJ JOOJ!");
            Console.WriteLine("(Você perdeu.)");
            Console.WriteLine("Pressione qualquer tecla para sair.");
            Console.ReadKey();

        }
    }
}
