﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace katia_faixaEtaria
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //Faça um algoritmo que armazene a idade de 1000 pessoas e calcule:
            // 1 - quantidade de pessoas em cada faixa etária;
            // 2 - o percentual de pessoas na primeira faixa etária em relação ao total;
            // 3 - o percentual de pessoas na ultima faixa etária em relação ao total;
            // Faixas Etárias: 1° = até 20 anos; 2° = 21 a 50 anos; 3° = 51 a 70 anos; 4° = acima de 70;

            Random idade = new Random();
            int[] faixaEtaria = new int[100];
            double prima = 0, segunda = 0, terca = 0, quarta = 0, total = 0;
            double percentUm = 0;
            double percentQuatro = 0;

            for (int i = 0; i < faixaEtaria.Length; i++)
            {
                faixaEtaria[i] = idade.Next(1, 100);

                if (faixaEtaria[i] <= 20)
                {
                    prima += 1;
                }

                else if (faixaEtaria[i] >= 21 && faixaEtaria[i] <= 50)
                {
                    segunda += 1;
                }

                else if (faixaEtaria[i] >= 51 && faixaEtaria[i] <= 70)
                {
                    terca += 1;
                }

                else
                    quarta += 1;
            }

            total = prima + segunda + terca + quarta;
            

            percentUm = prima / total * 100;
            percentQuatro = quarta / total * 100;

            Console.WriteLine($"O total de pessoas até 20 anos é de {prima} pessoas");
            Console.WriteLine($"O total de pessoas entre 21 e 50 anos é de {segunda} pessoas");
            Console.WriteLine($"O total de pessoas entre 51 e 70 anos é de {terca} pessoas.");
            Console.WriteLine($"E o total de pessoas acima de 70 anos é de {quarta} pessoas.\n");
            Console.WriteLine($"Jovens até 20 anos correspondem a {percentUm}% do total de pessoas.");
            Console.WriteLine($"Já idosos acima de 70 anos correspondem a {percentQuatro}% do total de pessoas.\n");

            Console.WriteLine("Pressione qualquer tecla para encerrar.");
            Console.ReadKey();
        }
    }
}
